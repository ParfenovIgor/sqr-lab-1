# Lab 1 -- Introduction to the quality gates

[![pipeline status](https://gitlab.com/ParfenovIgor/sqr-lab-1/badges/main/pipeline.svg)](https://gitlab.com/ParfenovIgor/sqr-lab-1/-/commits/main)

## Author

Igor Parfenov

Contact: [@Igor_Parfenov](https://t.me/Igor_Parfenov)

## Overview

The *Gitlab Runner* works on my machine. The script uses environmental variables. It consists of two parts:

* `build` - builds *Docker image* and pushed it into my account in [Docker Hub](hub.docker.com). 
* `deploy` - connects to my machine at [Innopolis VM](vm.innopolis.university) via *SSH* and pulls image from *Docker Hub*. It uses *--espose* option to make server accessible outside container.

![Curl](/screenshots/1.png)

The address of machine at *Innopolis VM* is `10.90.137.238`. It is only accessible from Innopolis network. The `10.90.137.238:8080/hello` is accessible.
